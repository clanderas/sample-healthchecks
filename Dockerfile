FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["SampleHealthChecks.csproj", ""]
RUN dotnet restore "SampleHealthChecks.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "SampleHealthChecks.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "SampleHealthChecks.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "SampleHealthChecks.dll"]