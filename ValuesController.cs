﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleHealthChecks
{
    [ApiController]
    [Route("api/values")]
    public class ValuesController: ControllerBase
    {
        [Route("")]
        [HttpGet]
        public ActionResult Get()
        {
            return Ok("Ok!");
        }
    }
}
